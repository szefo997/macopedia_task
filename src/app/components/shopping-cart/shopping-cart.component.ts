import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {Product} from '../../models/product.model';
import {ShoppingCart} from '../../models/shopping-cart.model';
import {ProductsDataService} from '../../services/products.service';
import {ShoppingCartService} from '../../services/shopping-cart.service';
import {CartItem} from '../../models/cart-item.model';
import {animate, style, transition, trigger} from '@angular/animations';

interface ICartItemWithProduct extends CartItem {
  product: Product;
  totalCost: number;
}

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({transform: 'translateX(-100%)', opacity: 0}),
          animate('200ms', style({transform: 'translateX(0)', opacity: 1}))
        ]),
        transition(':leave', [
          style({transform: 'translateX(0)', opacity: 1}),
          animate('200ms', style({transform: 'translateX(100%)', opacity: 0}))
        ])
      ]
    )
  ],
  styleUrls: ['./shopping-cart.component.scss']
})
export class ShoppingCartComponent implements OnInit, OnDestroy {
  public cartItems: ICartItemWithProduct[];
  public cart: Observable<ShoppingCart>;

  public total: number;

  private cartSubscription: Subscription;

  constructor(private productsService: ProductsDataService,
              private shoppingCartService: ShoppingCartService) {
  }

  ngOnInit() {
    this.cart = this.shoppingCartService.get();

    this.cartSubscription = this.cart.subscribe((cart) => {

      this.total = cart.grossTotal;

      this.productsService.all().subscribe((products) => {

        this.cartItems = cart.items
          .map((item) => {
            const product = products.find((p) => p.id === item.productId);
            return {
              ...item,
              product,
              totalCost: product.price * item.quantity
            };
          });
      });
    });

  }

  public quantityChange(item: CartItem, event: Event) {
    if (event === null) {
      item.quantity = 0;
    } else {
      item.quantity = event;
    }
    this.shoppingCartService.updateItem(item);
  }

  public onAdd(item: CartItem) {
    item.quantity = item.quantity + 1;
    this.shoppingCartService.updateItem(item);
  }

  public onRemove(item: CartItem) {
    item.quantity = item.quantity - 1;
    this.shoppingCartService.updateItem(item);
  }

  public ngOnDestroy(): void {
    if (this.cartSubscription) {
      this.cartSubscription.unsubscribe();
    }
  }

}
