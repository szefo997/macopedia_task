import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';

import {ShoppingCartComponent} from './shopping-cart.component';
import {ProductsDataService} from '../../services/products.service';
import * as sinon from 'sinon';
import {LocalStorageService, StorageService} from '../../services/storage.service';
import {ShoppingCartService} from '../../services/shopping-cart.service';
import {ShoppingCart} from '../../models/shopping-cart.model';
import {Observable, Observer} from 'rxjs';
import {FormsModule} from '@angular/forms';

class MockShoppingCartService {
  public unsubscriveCalled = false;
  public emptyCalled = false;
  private subscriptionObservable: Observable<ShoppingCart>;
  private subscriber: Observer<ShoppingCart>;
  private cart: ShoppingCart = new ShoppingCart();

  public constructor() {
    this.subscriptionObservable = new Observable<ShoppingCart>((observer: Observer<ShoppingCart>) => {
      this.subscriber = observer;
      observer.next(this.cart);
      return () => this.unsubscriveCalled = true;
    });
  }

  public get(): Observable<ShoppingCart> {
    return this.subscriptionObservable;
  }

  public dispatchCart(cart: ShoppingCart): void {
    this.cart = cart;
    if (this.subscriber) {
      this.subscriber.next(cart);
    }
  }
}

describe('ShoppingCartComponent', () => {
  let component: ShoppingCartComponent;
  let fixture: ComponentFixture<ShoppingCartComponent>;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [ShoppingCartComponent],
      imports: [
        FormsModule
      ],
      providers: [
        {provide: ProductsDataService, useValue: sinon.createStubInstance(ProductsDataService)},
        {provide: StorageService, useClass: LocalStorageService},
        {provide: ShoppingCartService, useClass: MockShoppingCartService}
      ]
    }).compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShoppingCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('should render gross total of shopping cart',
  //   async(inject([ShoppingCartService], (service: MockShoppingCartService) => {
  //     const compiled = fixture.debugElement.nativeElement;
  //     console.warn('compiled', compiled);
  //
  //     expect(compiled.querySelector('.js-cart-total').textContent.trim()).toContain('Suma: PLN0.00');
  //
  //     const newCart = new ShoppingCart();
  //     newCart.grossTotal = 1.5;
  //     service.dispatchCart(newCart);
  //     fixture.detectChanges();
  //     expect(compiled.querySelector('.js-cart-total').textContent.trim()).toContain('Suma: PLN1.50');
  //   })));


});
