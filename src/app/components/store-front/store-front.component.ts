import {Component, OnInit} from '@angular/core';
import {Product} from '../../models/product.model';
import {Observable} from 'rxjs';
import {ProductsDataService} from '../../services/products.service';
import {ShoppingCartService} from '../../services/shopping-cart.service';
import {animate, style, transition, trigger} from '@angular/animations';

const SORT_NAME = 'name';
const SORT_PRICE = 'price';

@Component({
  selector: 'app-store-front',
  templateUrl: './store-front.component.html',
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({opacity: 0}),
          animate('200ms', style({opacity: 1}))
        ]),
        transition(':leave', [
          style({opacity: 1}),
          animate('200ms', style({opacity: 0}))
        ])
      ]
    )
  ],
  styleUrls: ['./store-front.component.scss']
})
export class StoreFrontComponent implements OnInit {
  public products: Observable<Product[]>;
  public sorts: any[];
  public sortValue: string = null;

  public constructor(private productsService: ProductsDataService,
                     private shoppingCartService: ShoppingCartService) {
    this.sorts = [{
      text: 'Sortuj',
      value: null
    }, {
      text: 'Nazwa',
      value: SORT_NAME
    }, {
      text: 'Cena',
      value: SORT_PRICE
    }];
  }

  ngOnInit() {
    this.products = this.productsService.all();
  }

  addProductToCart(product: Product) {
    this.shoppingCartService.addItem(product, 1);
  }

  onSort(event: string) {
    if (event === SORT_NAME) {
      this.products = this.productsService.sortBy('name', this.products);
    } else if (event === SORT_PRICE) {
      this.products = this.productsService.sortBy('price', this.products);
    } else {
      this.products = this.productsService.sortBy('id', this.products);
    }
  }

}
