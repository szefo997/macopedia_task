import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';

import {StoreFrontComponent} from './store-front.component';
import {Product} from '../../models/product.model';
import {ProductsDataService} from '../../services/products.service';
import {Observable, Observer, of} from 'rxjs';
import {LocalStorageService, StorageService} from '../../services/storage.service';
import {ShoppingCartService} from '../../services/shopping-cart.service';
import {ShoppingCart} from '../../models/shopping-cart.model';
import {ShoppingCartComponent} from '../shopping-cart/shopping-cart.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import * as sinon from 'sinon';

const PRODUCT_1 = new Product(
  '1',
  'a',
  2
);

const PRODUCT_2 = new Product(
  '3',
  'b',
  1
);

const PRODUCT_3 = new Product(
  '2',
  'c',
  3
);

class MockProductDataService extends ProductsDataService {

  public all(): Observable<Product[]> {
    return of([PRODUCT_1, PRODUCT_2, PRODUCT_3]);
  }

}

class MockShoppingCartService {
  public unsubscriveCalled = false;

  private subscriptionObservable: Observable<ShoppingCart>;
  private subscriber: Observer<ShoppingCart>;
  private cart: ShoppingCart = new ShoppingCart();

  public constructor() {
    this.subscriptionObservable = new Observable<ShoppingCart>((observer: Observer<ShoppingCart>) => {
      this.subscriber = observer;
      observer.next(this.cart);
      return () => this.unsubscriveCalled = true;
    });
  }

  public addItem(product: Product, quantity: number): void {
  }

  public get(): Observable<ShoppingCart> {
    return this.subscriptionObservable;
  }

  public dispatchCart(cart: ShoppingCart): void {
    this.cart = cart;
    if (this.subscriber) {
      this.subscriber.next(cart);
    }
  }
}

describe('StoreFrontComponent', () => {
  let component: StoreFrontComponent;
  let fixture: ComponentFixture<StoreFrontComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ShoppingCartComponent,
        StoreFrontComponent
      ],
      imports: [
        BrowserModule,
        FormsModule,
        BrowserAnimationsModule,
        HttpClientModule
      ],
      providers: [
        {provide: ProductsDataService, useClass: MockProductDataService},
        {provide: StorageService, useClass: LocalStorageService},
        {provide: ShoppingCartService, useClass: MockShoppingCartService}
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreFrontComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display all the products', async(() => {
    const compiled = fixture.debugElement.nativeElement;
    const productElements = compiled.querySelectorAll('.js-li');

    expect(productElements.length).toEqual(3);

    expect(productElements[0].querySelector('.js-product-name').textContent.trim()).toEqual(PRODUCT_1.name);
    expect(productElements[0].querySelector('.js-product-price').textContent.trim()).toContain(PRODUCT_1.price);

    expect(productElements[1].querySelector('.js-product-name').textContent.trim()).toEqual(PRODUCT_2.name);
    expect(productElements[1].querySelector('.js-product-price').textContent.trim()).toContain(PRODUCT_2.price);

    expect(productElements[2].querySelector('.js-product-name').textContent.trim()).toEqual(PRODUCT_3.name);
    expect(productElements[2].querySelector('.js-product-price').textContent.trim()).toContain(PRODUCT_3.price);

  }));

  it('should add the product to the cart when add item button is clicked',
    async(inject([ShoppingCartService], (service: MockShoppingCartService) => {
      const addItemSpy = sinon.spy(service, 'addItem');

      const compiled = fixture.debugElement.nativeElement;
      const productElements = compiled.querySelectorAll('.js-li');

      productElements[0].querySelector('.js-btn-add').click();
      sinon.assert.calledOnce(addItemSpy);
      sinon.assert.calledWithExactly(addItemSpy, PRODUCT_1, 1);
    })));

});
