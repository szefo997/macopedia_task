import {CartItem} from './cart-item.model';

export class ShoppingCart {
  public items: CartItem[];
  public grossTotal: number;

  constructor() {
    this.items = [];
    this.grossTotal = 0;
  }
}
