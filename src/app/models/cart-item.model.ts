export class CartItem {
  public productId: string;
  public quantity;

  constructor(productId: string) {
    this.productId = productId;
    this.quantity = 0;
  }

}
