import {inject, TestBed} from '@angular/core/testing';
import {ProductsDataService} from './products.service';
import {Product} from '../models/product.model';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';


describe('ProductsService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        ProductsDataService
      ]
    });
  });

  it('should be injectable', inject([ProductsDataService], (service: ProductsDataService) => {
    expect(service).toBeTruthy();
  }));

  describe('all()', () => {

    it('should call the correct http endpoint', () => {
      const productsDataService = TestBed.get(ProductsDataService);
      const http = TestBed.get(HttpTestingController);

      const products = createProducts(5);
      let actualProducts = [];
      productsDataService.all().subscribe(_products => {
        actualProducts = _products;
      });

      http.expectOne('./assets/products.json').flush(products);

      expect(actualProducts).toEqual(products);
      expect(products.length).toBe(5);
      expect(products[0].id).toBe('0');
      expect(products[1].id).toBe('1');
    });

  });

});

function createProducts(count: number): Product[] {
  const products = [];
  for (let i = 0; i < count; i += 1) {
    products.push(new Product(
      i.toString(),
      `name ${i}`,
      i
    ));
  }
  return products;
}
