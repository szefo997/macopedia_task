import {Injectable} from '@angular/core';
import {ShoppingCart} from '../models/shopping-cart.model';
import {Product} from '../models/product.model';
import {ProductsDataService} from './products.service';
import {CartItem} from '../models/cart-item.model';
import {Observable, Observer} from 'rxjs';
import {StorageService} from './storage.service';

const CART_KEY = 'macopedia_cart';

@Injectable()
export class ShoppingCartService {
  private storage: Storage;
  private products: Product[];
  private subscriptionObservable: Observable<ShoppingCart>;

  private subscribers: Array<Observer<ShoppingCart>> = [];

  public constructor(private productService: ProductsDataService,
                     private storageService: StorageService) {

    this.storage = this.storageService.getLocalStorage();
    this.productService.all().subscribe((products) => this.products = products);

    this.subscriptionObservable = new Observable<ShoppingCart>((observer: Observer<ShoppingCart>) => {
      this.subscribers.push(observer);
      observer.next(this.retrieve());
      return () => {
        this.subscribers = this.subscribers.filter((obs) => obs !== observer);
      };
    });
  }

  public get(): Observable<ShoppingCart> {
    return this.subscriptionObservable;
  }

  public addItem(product: Product, quantity: number): void {
    const cart = this.retrieve();
    let item = cart.items.find((p) => p.productId === product.id);
    if (item === undefined) {
      item = new CartItem(product.id);
      cart.items.push(item);
    }

    item.quantity += quantity;
    cart.items = cart.items.filter((cartItem) => cartItem.quantity > 0);

    this.calculateCart(cart);
    this.save(cart);
    this.dispatch(cart);
  }

  public updateItem(item: CartItem) {
    const cart = this.retrieve();
    const _item = cart.items.find((p) => p.productId === item.productId);
    _item.quantity = item.quantity;
    cart.items = cart.items.filter((cartItem) => cartItem.quantity > 0);
    this.calculateCart(cart);
    this.save(cart);
    this.dispatch(cart);
  }

  private calculateCart(cart: ShoppingCart): void {
    cart.grossTotal = cart.items
      .map((item) => item.quantity * this.products.find((p) => p.id === item.productId).price)
      .reduce((previous, current) => previous + current, 0);
  }

  private retrieve(): ShoppingCart {
    const cart = new ShoppingCart();
    const storedCart = this.storage.getItem(CART_KEY);
    if (storedCart) {
      return JSON.parse(storedCart);
    }
    return cart;
  }

  private save(cart: ShoppingCart): void {
    this.storage.setItem(CART_KEY, JSON.stringify(cart));
  }

  private dispatch(cart: ShoppingCart): void {
    this.subscribers
      .forEach((sub) => {
        try {
          sub.next(cart);
        } catch (e) {}
      });
  }

}
