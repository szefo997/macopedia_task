import {Injectable} from '@angular/core';
import {CachingServiceBase} from './caching.service';
import {Observable} from 'rxjs';
import {Product} from '../models/product.model';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable()
export class ProductsDataService extends CachingServiceBase {

  private products: Observable<Product[]>;

  public constructor(private http: HttpClient) {
    super();
  }

  public all(): Observable<Product[]> {
    return this.cache<Product[]>(
      () => this.products,
      (val: Observable<Product[]>) => this.products = val,
      () => this.http.get<Product[]>('./assets/products.json').pipe(
        map(source => source.sort((a, b) => {
          if (a.id < b.id) {
            return -1;
          } else if (a.id > b.id) {
            return 1;
          } else {
            return 0;
          }
        })))
    );
  }

  public sortBy(sortValue: string, products: Observable<Product[]>) {
    const product = new Product('test', 'test', 1);
    if (sortValue && products && product.hasOwnProperty(sortValue)) {
      return products.pipe(
        map(source => source.sort((a, b) => {
          if (a[sortValue] < b[sortValue]) {
            return -1;
          } else if (a[sortValue] > b[sortValue]) {
            return 1;
          } else {
            return 0;
          }
        })));
    }
  }

}
