import {Injectable} from '@angular/core';
import 'rxjs/operators';

export abstract class StorageService {
  public abstract getLocalStorage(): Storage;
}

@Injectable()
export class LocalStorageService extends StorageService {

  getLocalStorage(): Storage {
    return localStorage;
  }

}
