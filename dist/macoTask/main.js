(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container {\n  padding: 0 15px;\n  max-width: 1280px;\n  margin: 0 auto; }\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: "\n    <app-header></app-header>\n    <div class=\"container\">\n      <router-outlet></router-outlet>\n    </div>\n  ",
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_store_front_store_front_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/store-front/store-front.component */ "./src/app/components/store-front/store-front.component.ts");
/* harmony import */ var _app_routing__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.routing */ "./src/app/app.routing.ts");
/* harmony import */ var _services_products_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./services/products.service */ "./src/app/services/products.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _components_header_header_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/header/header.component */ "./src/app/components/header/header.component.ts");
/* harmony import */ var _components_shopping_cart_shopping_cart_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/shopping-cart/shopping-cart.component */ "./src/app/components/shopping-cart/shopping-cart.component.ts");
/* harmony import */ var _services_shopping_cart_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./services/shopping-cart.service */ "./src/app/services/shopping-cart.service.ts");
/* harmony import */ var _services_storage_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./services/storage.service */ "./src/app/services/storage.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
                _components_store_front_store_front_component__WEBPACK_IMPORTED_MODULE_3__["StoreFrontComponent"],
                _components_header_header_component__WEBPACK_IMPORTED_MODULE_7__["HeaderComponent"],
                _components_shopping_cart_shopping_cart_component__WEBPACK_IMPORTED_MODULE_8__["ShoppingCartComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormsModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_12__["BrowserAnimationsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
                _app_routing__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"]
            ],
            providers: [
                _services_products_service__WEBPACK_IMPORTED_MODULE_5__["ProductsDataService"],
                _services_storage_service__WEBPACK_IMPORTED_MODULE_10__["LocalStorageService"],
                _services_shopping_cart_service__WEBPACK_IMPORTED_MODULE_9__["ShoppingCartService"],
                { provide: _services_storage_service__WEBPACK_IMPORTED_MODULE_10__["StorageService"], useClass: _services_storage_service__WEBPACK_IMPORTED_MODULE_10__["LocalStorageService"] }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routing.ts":
/*!********************************!*\
  !*** ./src/app/app.routing.ts ***!
  \********************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_store_front_store_front_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/store-front/store-front.component */ "./src/app/components/store-front/store-front.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot([
                    {
                        component: _components_store_front_store_front_component__WEBPACK_IMPORTED_MODULE_2__["StoreFrontComponent"],
                        path: '**'
                    }
                ])
            ]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/header/header.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/header/header.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"clear\">\n  <div class=\"top-bar\">\n    <div class=\"top-bar-left\">\n      <h2>Macopedia task</h2>\n    </div>\n  </div>\n</header>\n"

/***/ }),

/***/ "./src/app/components/header/header.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/components/header/header.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "header {\n  background-color: rgba(51, 51, 51, 0.9);\n  color: white;\n  box-shadow: 0 0 10px rgba(51, 51, 51, 0.6);\n  margin-bottom: 1rem;\n  padding: 0 1rem; }\n  header ul {\n    background-color: transparent; }\n"

/***/ }),

/***/ "./src/app/components/header/header.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/header/header.component.ts ***!
  \*******************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/components/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/components/header/header.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/components/shopping-cart/shopping-cart.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/components/shopping-cart/shopping-cart.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@enterAnimation] *ngIf=\"cartItems && cartItems.length > 0\" class=\"wrap-basket\">\n  <h3><span class=\"material-icons\">shopping_basket</span>Koszyk</h3>\n  <button type=\"button\"\n          (click)=\"expanded = !expanded\"\n          class=\"button success large text--bold js-btn-expand\">\n    {{expanded ? 'Ukryj koszyk' : 'Rozwiń koszyk'}}\n  </button>\n</div>\n\n<div *ngIf=\"expanded\" [@enterAnimation]>\n  <ul class=\"cart-list\">\n    <li *ngFor=\"let item of cartItems\">\n      <figure>\n        <div class=\"wrap-img\">\n          <img src=\"./assets/{{item.product.id}}.jpg\" class=\"product_image\"/>\n        </div>\n        <figcaption>\n          <ul>\n            <li>\n              <span class=\"text--bold\">{{item.product.name}}</span>\n            </li>\n            <li>\n              <input type=\"number\" min=\"0\" step=\"1\" name=\"quantity\" [ngModel]=\"item.quantity\"\n                     (ngModelChange)=\"quantityChange(item,  $event)\"\n                     [ngModelOptions]=\"{updateOn: 'blur'}\"/>\n              <span class=\"material-icons js-add\" (click)=\"onAdd(item)\">add</span>\n              <span class=\"material-icons js-remove\" (click)=\"onRemove(item)\">remove</span>\n            </li>\n            <li>\n              <span class=\"text--bold text--red\">{{item.totalCost | currency:'PLN' }}</span>\n            </li>\n          </ul>\n        </figcaption>\n      </figure>\n    </li>\n  </ul>\n  <h3 *ngIf=\"cartItems && cartItems.length > 0\" class=\"sum js-cart-total\">\n    Suma: {{total | currency:'PLN' }}\n  </h3>\n</div>\n"

/***/ }),

/***/ "./src/app/components/shopping-cart/shopping-cart.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/components/shopping-cart/shopping-cart.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".wrap-basket {\n  margin-bottom: 2rem; }\n  .wrap-basket span.material-icons {\n    margin-right: 0.5rem;\n    position: relative;\n    top: 4px; }\n  .cart-list ul,\n.cart-list {\n  padding: 0;\n  list-style: none;\n  margin: 0; }\n  .cart-list {\n  margin-bottom: 35px; }\n  .cart-list > li {\n    height: 100px;\n    margin: 10px 0;\n    border: 1px solid #EFEFEF; }\n  .cart-list > li:hover {\n      box-shadow: 0 0 5px rgba(50, 50, 50, 0.3); }\n  @media (max-width: 768px) {\n      .cart-list > li {\n        height: auto; } }\n  .cart-list .wrap-img {\n    position: relative;\n    width: 150px;\n    height: 100%;\n    display: inline-block;\n    max-height: 100px;\n    vertical-align: middle; }\n  @media (max-width: 768px) {\n      .cart-list .wrap-img {\n        display: block;\n        width: auto;\n        margin: 0 auto;\n        text-align: center;\n        padding-top: 10px; } }\n  .cart-list img {\n    max-height: 90px;\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%); }\n  @media (max-width: 768px) {\n      .cart-list img {\n        position: static;\n        -webkit-transform: none;\n                transform: none; } }\n  .cart-list figure {\n    height: 100%;\n    margin: 0; }\n  .cart-list figcaption {\n    display: inline-block;\n    vertical-align: middle; }\n  @media (max-width: 768px) {\n      .cart-list figcaption {\n        display: block; } }\n  .cart-list figcaption ul > li {\n      display: inline-block;\n      margin-right: 25px; }\n  @media (max-width: 768px) {\n        .cart-list figcaption ul > li {\n          display: block;\n          text-align: center;\n          margin: 5px 0 !important;\n          width: auto !important; } }\n  .cart-list figcaption ul > li:first-child {\n        margin-left: 15px;\n        width: 100px;\n        text-align: center; }\n  .cart-list figcaption ul > li:nth-child(2) {\n        width: 100px;\n        position: relative; }\n  .cart-list figcaption ul > li:nth-child(2) input[type=number] {\n          width: calc(100% - 34px);\n          text-align: right;\n          -webkit-appearance: none;\n          margin: 0; }\n  @media (max-width: 768px) {\n            .cart-list figcaption ul > li:nth-child(2) input[type=number] {\n              width: calc(50% - 34px); } }\n  @media (max-width: 576px) {\n            .cart-list figcaption ul > li:nth-child(2) input[type=number] {\n              width: calc(100% - 64px); } }\n  .cart-list figcaption ul > li:nth-child(2) input[type=number]::-webkit-inner-spin-button,\n        .cart-list figcaption ul > li:nth-child(2) input[type=number]::-webkit-outer-spin-button {\n          -webkit-appearance: none;\n          margin: 0; }\n  .cart-list figcaption ul > li:nth-child(2) span {\n          position: absolute;\n          right: 0;\n          cursor: pointer;\n          transition: opacity, color 200ms linear; }\n  @media (max-width: 768px) {\n            .cart-list figcaption ul > li:nth-child(2) span {\n              right: calc(25% - 34px); } }\n  @media (max-width: 576px) {\n            .cart-list figcaption ul > li:nth-child(2) span {\n              right: 0; } }\n  .cart-list figcaption ul > li:nth-child(2) span:nth-child(2) {\n            top: -8px; }\n  .cart-list figcaption ul > li:nth-child(2) span:last-child {\n            bottom: -8px; }\n  .cart-list figcaption ul > li:nth-child(2) span:hover {\n            opacity: 0.6; }\n  .cart-list figcaption ul > li:last-child {\n        margin-right: 0; }\n  h3.sum {\n  padding-left: 400px; }\n  @media (max-width: 768px) {\n    h3.sum {\n      padding-left: 0;\n      text-align: center; } }\n"

/***/ }),

/***/ "./src/app/components/shopping-cart/shopping-cart.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/components/shopping-cart/shopping-cart.component.ts ***!
  \*********************************************************************/
/*! exports provided: ShoppingCartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShoppingCartComponent", function() { return ShoppingCartComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_products_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/products.service */ "./src/app/services/products.service.ts");
/* harmony import */ var _services_shopping_cart_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/shopping-cart.service */ "./src/app/services/shopping-cart.service.ts");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
var __assign = (undefined && undefined.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ShoppingCartComponent = /** @class */ (function () {
    function ShoppingCartComponent(productsService, shoppingCartService) {
        this.productsService = productsService;
        this.shoppingCartService = shoppingCartService;
    }
    ShoppingCartComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.cart = this.shoppingCartService.get();
        this.cartSubscription = this.cart.subscribe(function (cart) {
            _this.total = cart.grossTotal;
            _this.productsService.all().subscribe(function (products) {
                _this.cartItems = cart.items
                    .map(function (item) {
                    var product = products.find(function (p) { return p.id === item.productId; });
                    return __assign({}, item, { product: product, totalCost: product.price * item.quantity });
                });
            });
        });
    };
    ShoppingCartComponent.prototype.quantityChange = function (item, event) {
        if (event === null) {
            item.quantity = 0;
        }
        else {
            item.quantity = event;
        }
        this.shoppingCartService.updateItem(item);
    };
    ShoppingCartComponent.prototype.onAdd = function (item) {
        item.quantity = item.quantity + 1;
        this.shoppingCartService.updateItem(item);
    };
    ShoppingCartComponent.prototype.onRemove = function (item) {
        item.quantity = item.quantity - 1;
        this.shoppingCartService.updateItem(item);
    };
    ShoppingCartComponent.prototype.ngOnDestroy = function () {
        if (this.cartSubscription) {
            this.cartSubscription.unsubscribe();
        }
    };
    ShoppingCartComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-shopping-cart',
            template: __webpack_require__(/*! ./shopping-cart.component.html */ "./src/app/components/shopping-cart/shopping-cart.component.html"),
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["trigger"])('enterAnimation', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["transition"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["style"])({ transform: 'translateX(-100%)', opacity: 0 }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["animate"])('200ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["style"])({ transform: 'translateX(0)', opacity: 1 }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["transition"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["style"])({ transform: 'translateX(0)', opacity: 1 }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["animate"])('200ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["style"])({ transform: 'translateX(100%)', opacity: 0 }))
                    ])
                ])
            ],
            styles: [__webpack_require__(/*! ./shopping-cart.component.scss */ "./src/app/components/shopping-cart/shopping-cart.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_products_service__WEBPACK_IMPORTED_MODULE_1__["ProductsDataService"],
            _services_shopping_cart_service__WEBPACK_IMPORTED_MODULE_2__["ShoppingCartService"]])
    ], ShoppingCartComponent);
    return ShoppingCartComponent;
}());



/***/ }),

/***/ "./src/app/components/store-front/store-front.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/components/store-front/store-front.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  <app-shopping-cart></app-shopping-cart>\n</div>\n<div>\n  <h3><span class=\"material-icons\">grid_on</span>Produkty</h3>\n  <select [ngModel]=\"sortValue\" class=\"js-sort\" (ngModelChange)=\"onSort($event)\">\n    <option [selected]=\"sortValue == s.value\" *ngFor=\"let s of sorts\" [ngValue]=\"s.value\">{{s.text}}</option>\n  </select>\n  <ul class=\"product-list\">\n    <li class=\"js-li\" [@enterAnimation] *ngFor=\"let product of products | async\">\n      <figure>\n        <div class=\"wrap-img\">\n          <img src=\"./assets/{{product.id}}.jpg\" class=\"product_image\"/>\n        </div>\n        <figcaption>\n            <span class=\"text--bold clear js-product-name\">\n              {{product.name}}\n            </span>\n          <small class=\"product_price clear js-product-price\">\n            {{product.price | currency:'PLN'}}\n          </small>\n          <button type=\"button\"\n                  class=\"button success text--bold js-btn-add\"\n                  (click)=\"addProductToCart(product)\">Dodaj do koszyka\n          </button>\n        </figcaption>\n      </figure>\n    </li>\n  </ul>\n</div>\n"

/***/ }),

/***/ "./src/app/components/store-front/store-front.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/components/store-front/store-front.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h3 {\n  display: inline-block; }\n  h3 > span {\n    position: relative;\n    top: 4px;\n    margin-right: 0.5rem; }\n  select {\n  font-size: 1.2rem;\n  display: inline-block;\n  margin-left: 1rem;\n  background-color: #fff; }\n  .product-list {\n  list-style-type: none;\n  margin: 0;\n  padding: 0;\n  text-align: center; }\n  .product-list > li {\n    display: inline-block;\n    width: calc(25% - 15px);\n    min-width: 220px;\n    padding: 1rem;\n    position: relative;\n    border: 1px solid #EFEFEF;\n    margin: 10px; }\n  @media (max-width: 992px) {\n      .product-list > li {\n        width: calc(33% - 14px); }\n        .product-list > li:nth-child(3n + 1) {\n          margin-left: 0; }\n        .product-list > li:nth-child(3n) {\n          margin-right: 0; } }\n  @media (max-width: 768px) {\n      .product-list > li {\n        width: calc(50% - 8px);\n        margin: 15px 0 !important; }\n        .product-list > li:nth-child(odd) {\n          margin-right: 15px !important; } }\n  @media (max-width: 576px) {\n      .product-list > li {\n        width: 100%;\n        margin: 15px 0 !important; } }\n  .product-list > li:hover {\n      box-shadow: 0 0 5px rgba(50, 50, 50, 0.3); }\n  @media (min-width: 992px) {\n      .product-list > li:nth-child(4n + 1) {\n        margin-left: 0; }\n      .product-list > li:nth-child(4n) {\n        margin-right: 0; } }\n  .product-list figure {\n    height: 360px;\n    width: 100%;\n    margin: 0; }\n  @media (max-width: 1200px) {\n      .product-list figure {\n        height: 320px; } }\n  @media (max-width: 768px) {\n      .product-list figure {\n        height: 260px; } }\n  .product-list .wrap-img {\n    height: 260px;\n    position: relative;\n    margin-bottom: 0.5rem; }\n  @media (max-width: 1200px) {\n      .product-list .wrap-img {\n        height: 220px; } }\n  @media (max-width: 768px) {\n      .product-list .wrap-img {\n        height: 160px; } }\n  .product-list figcaption {\n    height: 100px;\n    bottom: 0;\n    left: 0;\n    width: 100%; }\n  .product-list img {\n    max-height: 100%;\n    position: absolute;\n    top: 50%;\n    left: 50%;\n    -webkit-transform: translate(-50%, -50%);\n            transform: translate(-50%, -50%); }\n"

/***/ }),

/***/ "./src/app/components/store-front/store-front.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/store-front/store-front.component.ts ***!
  \*****************************************************************/
/*! exports provided: StoreFrontComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StoreFrontComponent", function() { return StoreFrontComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_products_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/products.service */ "./src/app/services/products.service.ts");
/* harmony import */ var _services_shopping_cart_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/shopping-cart.service */ "./src/app/services/shopping-cart.service.ts");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SORT_NAME = 'name';
var SORT_PRICE = 'price';
var StoreFrontComponent = /** @class */ (function () {
    function StoreFrontComponent(productsService, shoppingCartService) {
        this.productsService = productsService;
        this.shoppingCartService = shoppingCartService;
        this.sortValue = null;
        this.sorts = [{
                text: 'Sortuj',
                value: null
            }, {
                text: 'Nazwa',
                value: SORT_NAME
            }, {
                text: 'Cena',
                value: SORT_PRICE
            }];
    }
    StoreFrontComponent.prototype.ngOnInit = function () {
        this.products = this.productsService.all();
    };
    StoreFrontComponent.prototype.addProductToCart = function (product) {
        this.shoppingCartService.addItem(product, 1);
    };
    StoreFrontComponent.prototype.onSort = function (event) {
        if (event === SORT_NAME) {
            this.products = this.productsService.sortBy('name', this.products);
        }
        else if (event === SORT_PRICE) {
            this.products = this.productsService.sortBy('price', this.products);
        }
        else {
            this.products = this.productsService.sortBy('id', this.products);
        }
    };
    StoreFrontComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-store-front',
            template: __webpack_require__(/*! ./store-front.component.html */ "./src/app/components/store-front/store-front.component.html"),
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["trigger"])('enterAnimation', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["transition"])(':enter', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["style"])({ opacity: 0 }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["animate"])('200ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["style"])({ opacity: 1 }))
                    ]),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["transition"])(':leave', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["style"])({ opacity: 1 }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["animate"])('200ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_3__["style"])({ opacity: 0 }))
                    ])
                ])
            ],
            styles: [__webpack_require__(/*! ./store-front.component.scss */ "./src/app/components/store-front/store-front.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_products_service__WEBPACK_IMPORTED_MODULE_1__["ProductsDataService"],
            _services_shopping_cart_service__WEBPACK_IMPORTED_MODULE_2__["ShoppingCartService"]])
    ], StoreFrontComponent);
    return StoreFrontComponent;
}());



/***/ }),

/***/ "./src/app/models/cart-item.model.ts":
/*!*******************************************!*\
  !*** ./src/app/models/cart-item.model.ts ***!
  \*******************************************/
/*! exports provided: CartItem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartItem", function() { return CartItem; });
var CartItem = /** @class */ (function () {
    function CartItem(productId) {
        this.productId = productId;
        this.quantity = 0;
    }
    return CartItem;
}());



/***/ }),

/***/ "./src/app/models/product.model.ts":
/*!*****************************************!*\
  !*** ./src/app/models/product.model.ts ***!
  \*****************************************/
/*! exports provided: Product */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Product", function() { return Product; });
var Product = /** @class */ (function () {
    function Product(id, name, price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }
    return Product;
}());



/***/ }),

/***/ "./src/app/models/shopping-cart.model.ts":
/*!***********************************************!*\
  !*** ./src/app/models/shopping-cart.model.ts ***!
  \***********************************************/
/*! exports provided: ShoppingCart */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShoppingCart", function() { return ShoppingCart; });
var ShoppingCart = /** @class */ (function () {
    function ShoppingCart() {
        this.items = [];
        this.grossTotal = 0;
    }
    return ShoppingCart;
}());



/***/ }),

/***/ "./src/app/services/caching.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/caching.service.ts ***!
  \*********************************************/
/*! exports provided: CachingServiceBase */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CachingServiceBase", function() { return CachingServiceBase; });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");

var CachingServiceBase = /** @class */ (function () {
    function CachingServiceBase() {
    }
    CachingServiceBase.prototype.cache = function (getter, setter, retrieve) {
        var cached = getter();
        if (cached !== undefined) {
            return cached;
        }
        else {
            var val = retrieve();
            setter(val);
            return val;
        }
    };
    return CachingServiceBase;
}());



/***/ }),

/***/ "./src/app/services/products.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/products.service.ts ***!
  \**********************************************/
/*! exports provided: ProductsDataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsDataService", function() { return ProductsDataService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _caching_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./caching.service */ "./src/app/services/caching.service.ts");
/* harmony import */ var _models_product_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../models/product.model */ "./src/app/models/product.model.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProductsDataService = /** @class */ (function (_super) {
    __extends(ProductsDataService, _super);
    function ProductsDataService(http) {
        var _this = _super.call(this) || this;
        _this.http = http;
        return _this;
    }
    ProductsDataService.prototype.all = function () {
        var _this = this;
        return this.cache(function () { return _this.products; }, function (val) { return _this.products = val; }, function () { return _this.http.get('./assets/products.json').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (source) { return source.sort(function (a, b) {
            if (a.id < b.id) {
                return -1;
            }
            else if (a.id > b.id) {
                return 1;
            }
            else {
                return 0;
            }
        }); })); });
    };
    ProductsDataService.prototype.sortBy = function (sortValue, products) {
        var product = new _models_product_model__WEBPACK_IMPORTED_MODULE_2__["Product"]('test', 'test', 1);
        if (sortValue && products && product.hasOwnProperty(sortValue)) {
            return products.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (source) { return source.sort(function (a, b) {
                if (a[sortValue] < b[sortValue]) {
                    return -1;
                }
                else if (a[sortValue] > b[sortValue]) {
                    return 1;
                }
                else {
                    return 0;
                }
            }); }));
        }
    };
    ProductsDataService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], ProductsDataService);
    return ProductsDataService;
}(_caching_service__WEBPACK_IMPORTED_MODULE_1__["CachingServiceBase"]));



/***/ }),

/***/ "./src/app/services/shopping-cart.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/shopping-cart.service.ts ***!
  \***************************************************/
/*! exports provided: ShoppingCartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShoppingCartService", function() { return ShoppingCartService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_shopping_cart_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../models/shopping-cart.model */ "./src/app/models/shopping-cart.model.ts");
/* harmony import */ var _products_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./products.service */ "./src/app/services/products.service.ts");
/* harmony import */ var _models_cart_item_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/cart-item.model */ "./src/app/models/cart-item.model.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _storage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./storage.service */ "./src/app/services/storage.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CART_KEY = 'macopedia_cart';
var ShoppingCartService = /** @class */ (function () {
    function ShoppingCartService(productService, storageService) {
        var _this = this;
        this.productService = productService;
        this.storageService = storageService;
        this.subscribers = [];
        this.storage = this.storageService.getLocalStorage();
        this.productService.all().subscribe(function (products) { return _this.products = products; });
        this.subscriptionObservable = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Observable"](function (observer) {
            _this.subscribers.push(observer);
            observer.next(_this.retrieve());
            return function () {
                _this.subscribers = _this.subscribers.filter(function (obs) { return obs !== observer; });
            };
        });
    }
    ShoppingCartService.prototype.get = function () {
        return this.subscriptionObservable;
    };
    ShoppingCartService.prototype.addItem = function (product, quantity) {
        var cart = this.retrieve();
        var item = cart.items.find(function (p) { return p.productId === product.id; });
        if (item === undefined) {
            item = new _models_cart_item_model__WEBPACK_IMPORTED_MODULE_3__["CartItem"](product.id);
            cart.items.push(item);
        }
        item.quantity += quantity;
        cart.items = cart.items.filter(function (cartItem) { return cartItem.quantity > 0; });
        this.calculateCart(cart);
        this.save(cart);
        this.dispatch(cart);
    };
    ShoppingCartService.prototype.updateItem = function (item) {
        var cart = this.retrieve();
        var _item = cart.items.find(function (p) { return p.productId === item.productId; });
        _item.quantity = item.quantity;
        cart.items = cart.items.filter(function (cartItem) { return cartItem.quantity > 0; });
        this.calculateCart(cart);
        this.save(cart);
        this.dispatch(cart);
    };
    ShoppingCartService.prototype.calculateCart = function (cart) {
        var _this = this;
        cart.grossTotal = cart.items
            .map(function (item) { return item.quantity * _this.products.find(function (p) { return p.id === item.productId; }).price; })
            .reduce(function (previous, current) { return previous + current; }, 0);
    };
    ShoppingCartService.prototype.retrieve = function () {
        var cart = new _models_shopping_cart_model__WEBPACK_IMPORTED_MODULE_1__["ShoppingCart"]();
        var storedCart = this.storage.getItem(CART_KEY);
        if (storedCart) {
            return JSON.parse(storedCart);
        }
        return cart;
    };
    ShoppingCartService.prototype.save = function (cart) {
        this.storage.setItem(CART_KEY, JSON.stringify(cart));
    };
    ShoppingCartService.prototype.dispatch = function (cart) {
        this.subscribers
            .forEach(function (sub) {
            try {
                sub.next(cart);
            }
            catch (e) { }
        });
    };
    ShoppingCartService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_products_service__WEBPACK_IMPORTED_MODULE_2__["ProductsDataService"],
            _storage_service__WEBPACK_IMPORTED_MODULE_5__["StorageService"]])
    ], ShoppingCartService);
    return ShoppingCartService;
}());



/***/ }),

/***/ "./src/app/services/storage.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/storage.service.ts ***!
  \*********************************************/
/*! exports provided: StorageService, LocalStorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StorageService", function() { return StorageService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalStorageService", function() { return LocalStorageService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var StorageService = /** @class */ (function () {
    function StorageService() {
    }
    return StorageService;
}());

var LocalStorageService = /** @class */ (function (_super) {
    __extends(LocalStorageService, _super);
    function LocalStorageService() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LocalStorageService.prototype.getLocalStorage = function () {
        return localStorage;
    };
    LocalStorageService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], LocalStorageService);
    return LocalStorageService;
}(StorageService));



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/szefo/IdeaProjects/macoTask/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map